# ita-2019-java

# Create your first repository with name "ita-2019-java" in gitlab with README.MD

1. Clone this repository to your local machine, create a branch with name "feature/{CODE OF THIS TASK IN JIRA}_Create-first-repository", where the placeholder stands for the this task code (i.e. feature/I2-25_Create...) and the rest will be the summary of this task. 
2. Add some lines to the README.MD file, commit them with the commit message "{CODE OF THIS TASK IN JIRA} - Create first repository - readme finished" and push these changes to the remote repository. Then create a Merge request and add kubahejda as an assignee.
